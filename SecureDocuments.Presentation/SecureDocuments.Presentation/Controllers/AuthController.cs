﻿namespace SecureDocuments.Presentation.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using SecureDocuments.BLL.Dto.Internal;
    using SecureDocuments.BLL.Interfaces.Services;

    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IConfiguration _configuration;
        private IAuthService _authService;

        private string _jwtKey;

        public AuthController(IConfiguration configuration, IAuthService authService)
        {
            _configuration = configuration;
            _authService = authService;

            _jwtKey = _configuration.GetSection("Authentication:Token").Value;
        }

        [HttpPost]
        [Route("register")]
        public async Task<ActionResult<AuthenticationResponse>> Register(AuthenticationRequest request)
        {
            Task<AuthenticationResponse> response = _authService.Register(request, _jwtKey);

            return Ok(response);
        }

        [HttpPost]
        [Route("login")]
        public async Task<ActionResult<AuthenticationResponse>> Login(AuthenticationRequest request)
        {
            Task<AuthenticationResponse> response = _authService.Login(request, _jwtKey);

            return Ok(response);
        }
    }
}
