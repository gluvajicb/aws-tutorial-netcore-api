﻿namespace SecureDocuments.BLL.Factory
{
    using SecureDocuments.BLL.Dto;
    using SecureDocuments.BLL.Entities;

    public static class DocumentFactory
    {
        #region DB to Domain

        public static Document ToEntityDocument(this DAL.Model.Document dbDocument)
        {
            return new Document(dbDocument.Id,
                                dbDocument.Uid,
                                dbDocument.FilePath,
                                dbDocument.Name,
                                dbDocument.UserFk,
                                dbDocument.CreatedAt,
                                dbDocument.DeletedAt,
                                dbDocument.User.ToEntityUser());
        }

        #endregion

        #region DB to Dto

        public static DocumentDto ToDtoDocument(this DAL.Model.Document dbDocument)
        {
            return new DocumentDto(dbDocument.Uid,
                                   dbDocument.FilePath,
                                   dbDocument.Name,
                                   dbDocument.CreatedAt);
        }

        #endregion

        #region Domain to DB

        public static DAL.Model.Document ToEntityDocument(this Document entityDocument)
        {
            return new DAL.Model.Document(entityDocument.Id,
                                          entityDocument.Uid,
                                          entityDocument.FilePath,
                                          entityDocument.Name,
                                          entityDocument.UserFk,
                                          entityDocument.CreatedAt,
                                          entityDocument.DeletedAt);
        }

        #endregion

        #region Domain to Dto

        public static DocumentDto ToDtoDocument(this Document entityDocument)
        {
            return new DocumentDto(entityDocument.Uid,
                                   entityDocument.FilePath,
                                   entityDocument.Name,
                                   entityDocument.CreatedAt);
        }

        #endregion
    }
}
