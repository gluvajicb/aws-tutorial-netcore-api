﻿using SecureDocuments.BLL.Dto.Internal;

namespace SecureDocuments.BLL.Interfaces.Services
{
    public interface IAuthService
    {
        Task<AuthenticationResponse> Register(AuthenticationRequest request, string jwtKey);

        Task<AuthenticationResponse> Login(AuthenticationRequest request, string jwtKey);
    }
}
