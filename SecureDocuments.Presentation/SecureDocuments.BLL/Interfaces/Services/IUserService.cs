﻿using SecureDocuments.BLL.Dto;

namespace SecureDocuments.BLL.Interfaces.Services
{
    public interface IUserService
    {
        Task<UserDto> GetById(Guid uid);
    }
}
