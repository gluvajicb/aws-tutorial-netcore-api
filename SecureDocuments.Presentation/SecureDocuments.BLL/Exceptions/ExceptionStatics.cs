﻿namespace SecureDocuments.BLL.Exceptions
{
    public static class ExceptionStatics
    {
        public const int BadRequest = 400;
        public const int NotAuthorized = 401;
        public const int Forbidden = 401;
        public const int NotFound = 404;
    }
}
