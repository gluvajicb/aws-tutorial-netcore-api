﻿namespace SecureDocuments.BLL.Services
{
    using Microsoft.IdentityModel.Tokens;
    using SecureDocuments.BLL.Dto.Internal;
    using SecureDocuments.BLL.Entities;
    using SecureDocuments.BLL.Exceptions;
    using SecureDocuments.BLL.Factory;
    using SecureDocuments.BLL.Interfaces.Services;
    using SecureDocuments.DAL.Interfaces.Repository;
    using System.IdentityModel.Tokens.Jwt;
    using System.Security.Claims;
    using System.Security.Cryptography;
    using System.Text.RegularExpressions;

    public class AuthService : IAuthService
    {
        private IUserRepository _userRepository;

        public AuthService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<AuthenticationResponse> Register(AuthenticationRequest request, string jwtKey)
        {
            bool isEmailValid = IsEmailValid(request.Email);

            if (!isEmailValid)
            {
                throw new BusinessException("Email not valid!", ExceptionStatics.BadRequest);
            }

            DAL.Model.User? userCheck = await _userRepository.GetByEmail(request.Email);

            if (userCheck != null)
            {
                throw new BusinessException("User with this email already exists!", ExceptionStatics.BadRequest);
            }

            byte[] passwordHash;
            byte[] passwordSalt;

            CreatePasswordHash(request.Password, out passwordHash, out passwordSalt);

            string passwordHashString = Decode(passwordHash);
            string passwordSaltString = Decode(passwordSalt);

            DAL.Model.User userDb = new(Guid.NewGuid(),
                                        request.Email,
                                        passwordHashString,
                                        passwordSaltString);

            User createdUser = _userRepository.Create(userDb).ToEntityUser();

            string token = CreateToken(createdUser, jwtKey);

            return new AuthenticationResponse(token);
        }

        public async Task<AuthenticationResponse> Login(AuthenticationRequest request, string jwtKey)
        {
            DAL.Model.User? userDb = await _userRepository.GetByEmail(request.Email);

            if (userDb == null)
            {
                throw new BusinessException("User with this email does not exist!", ExceptionStatics.BadRequest);
            }

            byte[] encodedPasswordHash = Encode(userDb.Password);
            byte[] encodedPasswordSalt = Encode(userDb.PasswordSalt);

            if (VerifyPasswordHash(userDb.Password, encodedPasswordHash, encodedPasswordSalt))
            {
                User user = userDb.ToEntityUser();

                string token = CreateToken(user, jwtKey);

                return new AuthenticationResponse(token);
            }
            else
            {
                throw new BusinessException("Sign in credentials validation!", ExceptionStatics.BadRequest);
            }
        }

        #region Additional Logic

        private string CreateToken(User user, string jwtKey)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.Email)
            };

            SymmetricSecurityKey symmetricSecurityKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(jwtKey));

            SigningCredentials credentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha512Signature);

            JwtSecurityToken token = new JwtSecurityToken
            (
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: credentials
            );

            string jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt;
        }

        private void CreatePasswordHash(string password,
                                        out byte[] passwordHash,
                                        out byte[] passwordSalt)
        {
            using (HMACSHA512 hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private string Decode(byte[] bytes)
        {
            return Convert.ToBase64String(bytes);
        }

        private byte[] Encode(string bytes)
        {
            return Convert.FromBase64String(bytes);
        }

        private bool VerifyPasswordHash(string password,
                                        byte[] passwordHash,
                                        byte[] passwordSalt)
        {
            using (HMACSHA512 hmac = new HMACSHA512(passwordSalt))
            {
                byte[] computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));

                return computedHash == passwordHash;
            }
        }

        private static bool IsEmailValid(string email)
        {
            string regex = @"^[^@\s]+@[^@\s]+\.(com|net|org|gov)$";

            return Regex.IsMatch(email, regex, RegexOptions.IgnoreCase);
        }

        #endregion
    }
}
