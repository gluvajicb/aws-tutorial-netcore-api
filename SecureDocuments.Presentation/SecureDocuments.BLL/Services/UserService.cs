﻿namespace SecureDocuments.BLL.Services
{
    using SecureDocuments.BLL.Dto;
    using SecureDocuments.BLL.Factory;
    using SecureDocuments.BLL.Interfaces.Services;
    using SecureDocuments.DAL.Interfaces.Repository;
    using System;
    using System.Threading.Tasks;

    public class UserService : IUserService
    {
        private IUserRepository _repository;

        public UserService(IUserRepository repository)
        {
            _repository = repository;
        }

        public async Task<UserDto> GetById(Guid uid)
        {
            DAL.Model.User dbUser = await _repository.GetById(uid);
            UserDto result = dbUser.ToDtoUser();

            return result;
        }
    }
}
