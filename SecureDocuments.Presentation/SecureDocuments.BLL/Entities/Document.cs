﻿namespace SecureDocuments.BLL.Entities
{
    using System;

    public class Document
    {
        public int Id { get; set; }

        public Guid Uid { get; set; }

        public string FilePath { get; set; }

        public string Name { get; set; }

        public int UserFk { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? DeletedAt { get; set; }

        public User User { get; set; }

        public Document(int id,
                        Guid uid,
                        string filePath,
                        string name,
                        int userFk,
                        DateTime createdAt,
                        DateTime? deletedAt,
                        User user)
        {
            Id = id;
            Uid = uid;
            FilePath = filePath;
            Name = name;
            UserFk = userFk;
            CreatedAt = createdAt;
            DeletedAt = deletedAt;
            User = user;
        }
    }
}
