﻿namespace SecureDocuments.BLL.Dto.Internal
{
    public class AuthenticationRequest
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
