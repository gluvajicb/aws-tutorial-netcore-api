﻿namespace SecureDocuments.BLL.Dto.Internal
{
    public class AuthenticationResponse
    {
        public string Token { get; set; }

        public AuthenticationResponse(string token)
        {
            Token = token;
        }
    }
}
