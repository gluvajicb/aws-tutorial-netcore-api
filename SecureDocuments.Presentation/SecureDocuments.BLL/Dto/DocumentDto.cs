﻿namespace SecureDocuments.BLL.Dto
{
    using System;

    public class DocumentDto
    {
        public Guid Uid { get; set; }

        public string FilePath { get; set; }

        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }

        public DocumentDto(Guid uid,
                           string filePath,
                           string name,
                           DateTime createdAt)
        {
            Uid = uid;
            FilePath = filePath;
            Name = name;
            CreatedAt = createdAt;
        }
    }
}
