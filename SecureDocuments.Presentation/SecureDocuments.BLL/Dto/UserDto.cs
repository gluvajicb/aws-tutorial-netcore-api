﻿namespace SecureDocuments.BLL.Dto
{
    using System;

    public class UserDto
    {
        public Guid Uid { get; set; }

        public string Email { get; set; }


        public UserDto(Guid uid,
                       string email)
        {
            Uid = uid;
            Email = email;
        }
    }
}
