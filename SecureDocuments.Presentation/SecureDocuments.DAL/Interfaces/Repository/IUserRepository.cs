﻿namespace SecureDocuments.DAL.Interfaces.Repository
{
    using SecureDocuments.DAL.Model;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IUserRepository
    {
        User Create(User user);

        Task<List<User>> GetAll();

        Task<User> GetById(Guid uid);

        Task<User> GetByEmail(string email);

        User Update(User user);

        void Delete(Guid uid);
    }
}
