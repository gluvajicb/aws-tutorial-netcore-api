﻿namespace SecureDocuments.DAL.Model
{
    using System;
    using System.Collections.Generic;

    public partial class User
    {
        public User()
        {
            Document = new HashSet<Document>();
        }

        public int Id { get; set; }
        public Guid Uid { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public string? ResetToken { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }

        public virtual ICollection<Document> Document { get; set; }

        public User(int id,
                    Guid uid,
                    string email,
                    string password,
                    string passwordSalt,
                    string? resetToken,
                    DateTime createdAt,
                    DateTime? deletedAt)
        {
            Id = id;
            Uid = uid;
            Email = email;
            Password = password;
            PasswordSalt = passwordSalt;
            ResetToken = resetToken;
            CreatedAt = createdAt;
            DeletedAt = deletedAt;
        }

        public User(Guid uid,
                    string email,
                    string password,
                    string passwordSalt)
        {
            Uid = uid;
            Email = email;
            Password = password;
            PasswordSalt = passwordSalt;
        }
    }
}
