﻿namespace SecureDocuments.DAL.UnitOfWork
{
    using SecureDocuments.DAL.Model;

    public class UnitOfWork
    {
        public SecureDocumentsContext Context { get; }

        public UnitOfWork()
        {
            Context = new SecureDocumentsContext();
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
